import 'dart:convert';
import 'dart:core';
import 'package:http/http.dart' as http;

class Report {
  int projectNumber;
  String projectName;
  int mhNumber;
  String stationOrOffset;
  String inspector;
  String inspectionDate;
  String utilityType;
  String structureType;
  String structureConstructionType;
  String condition;
  String precastCone;
  String flatSlabTop;
  String pouredConcInv;
  double adjRingHeight;
  String openingOrientation;
  String maintenanceRequired;
  double rimElevation;
  double insideDiameter;
  String notes;
  String castingType;
  String lidType;
  String bicycleProof;
  
  Report({
    this.projectNumber,
    this.projectName,
    this.mhNumber,
    this.stationOrOffset,
    this.inspector,
    this.inspectionDate,
    this.utilityType,
    this.structureType,
    this.structureConstructionType,
    this.condition,
    this.precastCone,
    this.flatSlabTop,
    this.pouredConcInv,
    this.adjRingHeight,
    this.openingOrientation,
    this.maintenanceRequired,
    this.rimElevation,
    this.insideDiameter,
    this.notes,
    this.castingType,
    this.lidType,
    this.bicycleProof,
  });

  Map<String, dynamic> toMap() {
    return {
      'projectNumber': projectNumber,
      'projectName': projectName,
      'mhNumber': mhNumber,
      'stationOrOffset': stationOrOffset,
      'inspector': inspector,
      'inspectionDate': inspectionDate,
      'utilityType': utilityType,
      'structureType': structureType,
      'structureConstructionType': structureConstructionType,
      'condition': condition,
      'precastCone': precastCone,
      'flatSlabTop': flatSlabTop,
      'pouredConcInv': pouredConcInv,
      'adjRingHeight': adjRingHeight,
      'openingOrientation': openingOrientation,
      'maintenanceRequired': maintenanceRequired,
      'rimElevation': rimElevation,
      'insideDiameter': insideDiameter,
      'notes': notes,
      'castingType': castingType,
      'lidType': lidType,
      'bicycleProof': bicycleProof,
    };
  }

  factory Report.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return Report(
      projectNumber: map['projectNumber'],
      projectName: map['projectName'],
      mhNumber: map['mhNumber'],
      stationOrOffset: map['stationOrOffset'],
      inspector: map['inspector'],
      inspectionDate: map['date'],
      utilityType: map['utilityType'],
      structureType: map['structureType'],
      structureConstructionType: map['structureConstructionType'],
      condition: map['condition'],
      precastCone: map['precastCone'],
      flatSlabTop: map['flatSlabTop'],
      pouredConcInv: map['pouredConcInv'],
      adjRingHeight: map['adjRingHeight'],
      openingOrientation: map['orientation'],
      maintenanceRequired: map['maintenanceRequired'],
      rimElevation: map['ringElevation'],
      insideDiameter: map['insideDiameter'],
      notes: map['notes'],
      castingType: map['castingType'],
      lidType: map['lidType'],
      bicycleProof: map['bicycleProof'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Report.fromJson(String source) => Report.fromMap(json.decode(source));
}
