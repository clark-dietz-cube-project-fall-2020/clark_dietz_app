import 'package:flutter/material.dart';
import 'package:clark_dietz_app/pages/project-form.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Clark Dietz App Demo',
      home: ProjectForm(),
    );
  }
}
