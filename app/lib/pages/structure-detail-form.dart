import 'package:flutter/material.dart';
import 'measurement-form.dart';
import '../models/report.dart';

class StructureDetailForm extends StatefulWidget {
  final Report report;

  StructureDetailForm(this.report);

  @override
  _StructureDetailFormState createState() => _StructureDetailFormState();
}

class _StructureDetailFormState extends State<StructureDetailForm> {
  final _formState = GlobalKey<FormState>();

  final _precastConeTypes = ["Concentric", "Eccentric"]
      .map((type) => DropdownMenuItem(
            child: Text(type),
            value: type,
          ))
      .toList();

  final _flatSlabTopOptions = ["Yes", "No"]
      .map((type) => DropdownMenuItem(
            child: Text(type),
            value: type,
          ))
      .toList();

  final _pouredConcInvOptions = ["Yes", "No"]
      .map((type) => DropdownMenuItem(
            child: Text(type),
            value: type,
          ))
      .toList();

  String _precastCone;
  String _flatSlabTop;
  String _pouredConcInv;
  final _adjRingHeightController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Utility Structure Info")),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 24, vertical: 36),
          child: Form(
            key: _formState,
            child: Column(
              children: <Widget>[
                DropdownButtonFormField(
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(8.0),
                    labelText: "Precast Cone",
                  ),
                  items: _precastConeTypes,
                  value: _precastCone,
                  onChanged: (value) {
                    setState(() {
                      _precastCone = value;
                    });
                  },
                  isExpanded: true,
                ),
                DropdownButtonFormField(
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(8.0),
                    labelText: "Flat Slab Top",
                  ),
                  items: _flatSlabTopOptions,
                  value: _flatSlabTop,
                  onChanged: (value) {
                    setState(() {
                      _flatSlabTop = value;
                    });
                  },
                  isExpanded: true,
                ),
                DropdownButtonFormField(
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(8.0),
                    labelText: "Poured Conc. Inv.",
                  ),
                  items: _pouredConcInvOptions,
                  value: _pouredConcInv,
                  onChanged: (value) {
                    setState(() {
                      _pouredConcInv = value;
                    });
                  },
                  isExpanded: true,
                ),
                TextFormField(
                  controller: _adjRingHeightController,
                  decoration: const InputDecoration(
                    labelText: 'Adj. Ring Height (in)',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter a value';
                    }
                    return null;
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: RaisedButton(
                    onPressed: () {
                      widget.report.precastCone = _precastCone;
                      widget.report.flatSlabTop = _flatSlabTop;
                      widget.report.pouredConcInv = _pouredConcInv;
                      widget.report.adjRingHeight = double.parse(_adjRingHeightController.text);

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => MeasurementForm(widget.report),
                        ),
                      );
                    },
                    child: Text('Next'),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
