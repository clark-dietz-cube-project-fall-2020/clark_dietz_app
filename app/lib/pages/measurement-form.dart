import 'package:flutter/material.dart';
import 'casting-type-form.dart';
import '../models/report.dart';

class MeasurementForm extends StatefulWidget {
  final Report report;

  MeasurementForm(this.report);

  @override
  _MeasurementFormState createState() => _MeasurementFormState();
}

class _MeasurementFormState extends State<MeasurementForm> {
  final _formState = GlobalKey<FormState>();

  final _orientations = [
    "North",
    "Northeast",
    "East",
    "Southeast",
    "South",
    "Southwest",
    "West",
    "Northwest",
  ]
      .map((type) => DropdownMenuItem(
            child: Text(type),
            value: type,
          ))
      .toList();

  final _maintenanceOptions = ["Yes", "No"]
      .map((type) => DropdownMenuItem(
            child: Text(type),
            value: type,
          ))
      .toList();

  String _orientation;
  String _maintenance;
  final _rimElevationFtController = TextEditingController();
  final _rimElevationInController = TextEditingController();
  final _insideDiameterFtController = TextEditingController();
  final _insideDiameterInController = TextEditingController();
  final _notesController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Measurement Info")),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 36),
        child: Form(
          key: _formState,
          child: Column(
            children: <Widget>[
              DropdownButtonFormField(
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(8.0),
                  labelText: "Opening Orientation",
                ),
                items: _orientations,
                value: _orientation,
                onChanged: (value) {
                  setState(() {
                    _orientation = value;
                  });
                },
                isExpanded: true,
              ),
              DropdownButtonFormField(
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(8.0),
                  labelText: "Maintenance Needed",
                ),
                items: _maintenanceOptions,
                value: _maintenance,
                onChanged: (value) {
                  setState(() {
                    _maintenance = value;
                  });
                },
                isExpanded: true,
              ),
              TextFormField(
                controller: _rimElevationFtController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(8.0),
                  labelText: "Rim Elevation '",
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter a value';
                  }
                  return null;
                },
              ),
              TextFormField(
                controller: _rimElevationInController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(8.0),
                  labelText: "Rim Elevation \"",
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter a value';
                  }
                  return null;
                },
              ),
              TextFormField(
                controller: _insideDiameterFtController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(8.0),
                  labelText: "Inside Diameter '",
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter a value';
                  }
                  return null;
                },
              ),
              TextFormField(
                controller: _insideDiameterInController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(8.0),
                  labelText: "Inside Diameter \"",
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter a value';
                  }
                  return null;
                },
              ),
              TextFormField(
                controller: _notesController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(8.0),
                  labelText: "Notes",
                ),
                minLines: 4,
                maxLines: 4,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: RaisedButton(
                  onPressed: () {
                    widget.report.openingOrientation = _orientation;
                    widget.report.maintenanceRequired = _maintenance;

                    widget.report.rimElevation =
                        double.parse(_rimElevationFtController.text) * 12 +
                            double.parse(_rimElevationInController.text);

                    widget.report.insideDiameter =
                        double.parse(_insideDiameterFtController.text) * 12 +
                            double.parse(_insideDiameterInController.text);

                    widget.report.notes = _notesController.text;

                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CastingTypeForm(widget.report),
                      ),
                    );
                  },
                  child: Text('Next'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
