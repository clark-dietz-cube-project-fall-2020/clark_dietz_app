import 'package:flutter/material.dart';
import 'finished-page.dart';
import 'package:http/http.dart' as http;
import '../models/report.dart';

class CastingTypeForm extends StatefulWidget {
  final Report report;

  CastingTypeForm(this.report);

  @override
  _CastingTypeFormState createState() => _CastingTypeFormState();
}

class _CastingTypeFormState extends State<CastingTypeForm> {
  final _formState = GlobalKey<FormState>();

  final _castingTypes =
      ["Type 1", "Type 3", "Type 6", "Type 8", "Type 9", "Type 15", "Type 37", "Other"]
          .map((type) => DropdownMenuItem(
                child: Text(type),
                value: type,
              ))
          .toList();

  final _lidTypes = ["Open", "Closed"]
      .map((type) => DropdownMenuItem(
            child: Text(type),
            value: type,
          ))
      .toList();

  final _bicycleProofOptions = ["Yes", "No"]
      .map((type) => DropdownMenuItem(
            child: Text(type),
            value: type,
          ))
      .toList();

  String _castingType;
  String _lidType;
  String _bicycleProof;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Casting Type Info")),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 24, vertical: 36),
          child: Form(
            key: _formState,
            child: Column(
              children: <Widget>[
                DropdownButtonFormField(
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(8.0),
                    labelText: "Casting Type",
                  ),
                  items: _castingTypes,
                  value: _castingType,
                  onChanged: (value) {
                    setState(() {
                      _castingType = value;
                    });
                  },
                  isExpanded: true,
                ),
                DropdownButtonFormField(
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(8.0),
                    labelText: "Lid",
                  ),
                  items: _lidTypes,
                  value: _lidType,
                  onChanged: (value) {
                    setState(() {
                      _lidType = value;
                    });
                  },
                  isExpanded: true,
                ),
                DropdownButtonFormField(
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(8.0),
                    labelText: "Bicycle Proof",
                  ),
                  items: _bicycleProofOptions,
                  value: _bicycleProof,
                  onChanged: (value) {
                    setState(() {
                      _bicycleProof = value;
                    });
                  },
                  isExpanded: true,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: RaisedButton(
                    onPressed: () {
                      widget.report.castingType = _castingType;
                      widget.report.lidType = _lidType;
                      widget.report.bicycleProof = _bicycleProof;

                      print(widget.report.toMap());

                      http
                          .post("http://localhost:3000",
                              headers: {"Content-Type": "application/json"},
                              body: widget.report.toJson())
                          .then((response) {
                        if (response.statusCode == 200) {
                          Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(builder: (context) => FinishedPage()),
                            (Route<dynamic> route) => false,
                          );
                        }
                      });
                    },
                    child: Text('Submit Report'),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
