import 'package:flutter/material.dart';
import 'project-form.dart';

class FinishedPage extends StatefulWidget {
  @override
  _FinishedPageState createState() => _FinishedPageState();
}

class _FinishedPageState extends State<FinishedPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Finished Report")),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 36),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 64.0),
              Text("Finished", style: TextStyle(fontSize: 40.0), textAlign: TextAlign.center,),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: RaisedButton(
                  onPressed: () => Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => ProjectForm()),
                    (Route<dynamic> route) => false,
                  ),
                  child: Text('New report'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
