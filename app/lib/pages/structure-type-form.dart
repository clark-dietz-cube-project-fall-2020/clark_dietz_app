import 'package:flutter/material.dart';
import 'structure-detail-form.dart';
import '../models/report.dart';

class StructureTypeForm extends StatefulWidget {
  final Report report;

  StructureTypeForm(this.report);

  @override
  _StructureTypeFormState createState() => _StructureTypeFormState();
}

class _StructureTypeFormState extends State<StructureTypeForm> {
  final _formState = GlobalKey<FormState>();

  final _utilityTypes = [
    "Storm",
    "Sanitary",
    "Water",
    "Telephone",
    "Steam",
    "Fiber Optic",
    "Electrical",
    "Traffic Signal",
    "Other"
  ]
      .map((type) => DropdownMenuItem(
            child: Text(type),
            value: type,
          ))
      .toList();

  final _structureTypes = ["Manhole", "Catch Basin", "Inlet", "Vault", "Other"]
      .map((type) => DropdownMenuItem(
            child: Text(type),
            value: type,
          ))
      .toList();

  final _structureConstructionTypes = ["Precast", "Block", "Brick", "Other"]
      .map((type) => DropdownMenuItem(
            child: Text(type),
            value: type,
          ))
      .toList();

  final _conditions = ["Good", "Poor"]
      .map((type) => DropdownMenuItem(
            child: Text(type),
            value: type,
          ))
      .toList();

  String _utilityType;
  String _structureType;
  String _structureConstructionType;
  String _condition;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Utility Structure Info")),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 24, vertical: 36),
          child: Form(
            key: _formState,
            child: Column(
              children: <Widget>[
                DropdownButtonFormField(
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(8.0),
                    labelText: "Utility Type",
                  ),
                  items: _utilityTypes,
                  value: _utilityType,
                  onChanged: (value) {
                    setState(() {
                      _utilityType = value;
                    });
                  },
                  isExpanded: true,
                ),
                DropdownButtonFormField(
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(8.0),
                    labelText: "Structure Type",
                  ),
                  items: _structureTypes,
                  value: _structureType,
                  onChanged: (value) {
                    setState(() {
                      _structureType = value;
                    });
                  },
                  isExpanded: true,
                ),
                DropdownButtonFormField(
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(8.0),
                    labelText: "Structure Construction Type",
                  ),
                  items: _structureConstructionTypes,
                  value: _structureConstructionType,
                  onChanged: (value) {
                    setState(() {
                      _structureConstructionType = value;
                    });
                  },
                  isExpanded: true,
                ),
                DropdownButtonFormField(
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(8.0),
                    labelText: "Condition",
                  ),
                  items: _conditions,
                  value: _condition,
                  onChanged: (value) {
                    setState(() {
                      _condition = value;
                    });
                  },
                  isExpanded: true,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: RaisedButton(
                    onPressed: () {
                      widget.report.utilityType = _utilityType;
                      widget.report.structureType = _structureType;
                      widget.report.structureConstructionType = _structureConstructionType;
                      widget.report.condition = _condition;

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => StructureDetailForm(widget.report),
                        ),
                      );
                    },
                    child: Text('Next'),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
