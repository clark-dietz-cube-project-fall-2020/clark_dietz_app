import 'package:flutter/material.dart';
import 'structure-type-form.dart';
import '../models/report.dart';

class ReportInfoForm extends StatefulWidget {
  final Report report;

  ReportInfoForm(this.report);

  @override
  _ReportInfoFormState createState() => _ReportInfoFormState();
}

class _ReportInfoFormState extends State<ReportInfoForm> {
  final _formState = GlobalKey<FormState>();

  TextEditingController _mhController = TextEditingController();
  TextEditingController _stationController = TextEditingController();
  TextEditingController _inspectorController = TextEditingController();
  TextEditingController _dateController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Project Info")),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 24, vertical: 36),
          child: Form(
            key: _formState,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                TextFormField(
                  controller: _mhController,
                  decoration: const InputDecoration(
                    labelText: 'MH #',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter some text';
                    }

                    return null;
                  },
                ),
                TextFormField(
                  controller: _stationController,
                  decoration: const InputDecoration(
                    labelText: 'Station/Offset',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter some text';
                    }

                    return null;
                  },
                ),
                TextFormField(
                  controller: _inspectorController,
                  decoration: const InputDecoration(
                    labelText: 'Inspector',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter some text';
                    }

                    return null;
                  },
                ),
                TextFormField(
                  controller: _dateController,
                  decoration: const InputDecoration(
                    labelText: 'Date (MM/DD/YY)',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter some text';
                    }

                    return null;
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: RaisedButton(
                    onPressed: () {
                      widget.report.mhNumber = int.parse(_mhController.text);
                      widget.report.stationOrOffset = _stationController.text;
                      widget.report.inspector = _inspectorController.text;
                      widget.report.inspectionDate = _dateController.text;

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => StructureTypeForm(widget.report),
                        ),
                      );
                    },
                    child: Text('Next'),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
