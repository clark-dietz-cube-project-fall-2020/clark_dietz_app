const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');

const config = require('../config/server.js');
const database = require('./database.js');
const router = require('./router.js');

let server;

function initialize() {
    return new Promise((resolve, reject) => {
        const app = express();
        server = http.createServer(app);

        app.use(morgan('combined'));
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: false }));
        app.use('/', router);


        server.listen(config.port)
            .on('listening', () => {
                console.log(`Server listening on port ${config.port}`);

                resolve();
            })
            .on('error', err => {
                reject(err);
            });
    });
}

function close() {
    return new Promise((resolve, reject) => {
        server.close((err) => {
            if (err) {
                reject(err);
                return;
            }

            resolve();
        });
    });
}

module.exports.initialize = initialize;
module.exports.close = close;