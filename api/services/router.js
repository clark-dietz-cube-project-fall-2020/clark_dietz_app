const express = require('express');
const database = require('./database.js');

const router = new express.Router();

/* GET */
router.get('/', async (req, res) => {
    const result = await database.execute('select user, systimestamp from dual');
    const user = result.rows[0].USER;
    const date = result.rows[0].SYSTIMESTAMP;

    res.end(`DB user: ${user}\nDate: ${date}`);
});

/* POST */
router.post('/',  function (req, res) {
    const report = {
        projectNumber: req.body.projectNumber,
        projectName: req.body.projectName,
        mhNumber: req.body.mhNumber,
        stationOrOffset: req.body.stationOrOffset,
        inspector: req.body.inspector,
        inspectionDate: req.body.inspectionDate,
        utilityType: req.body.utilityType,
        structureType: req.body.structureType,
        structureConstructionType: req.body.structureConstructionType,
        condition: req.body.condition,
        precastCone: req.body.precastCone,
        flatSlabTop: req.body.flatSlabTop,
        pouredConcInv: req.body.pouredConcInv,
        adjRingHeight: req.body.adjRingHeight,
        openingOrientation: req.body.openingOrientation,
        maintenanceRequired: req.body.maintenanceRequired,
        rimElevation: req.body.rimElevation,
        insideDiameter: req.body.insideDiameter,
        notes: req.body.notes,
        castingType: req.body.castingType,
        lidType: req.body.lidType,
        bicycleProof: req.body.bicycleProof
    };

    console.log(report);

    const sql = `INSERT INTO Report VALUES(
        :projectNumber,
        :projectName,
        :mhNumber,
        :stationOrOffset,
        :inspector,
        :inspectionDate,
        :utilityType,
        :structureType,
        :structureConstructionType,
        :condition,
        :precastCone,
        :flatSlabTop,
        :pouredConcInv,
        :adjRingHeight,
        :openingOrientation,
        :maintenanceRequired,
        :rimElevation,
        :insideDiameter,
        :notes,
        :castingType,
        :lidType,
        :bicycleProof
     )`;

    database.execute(sql, report).then((_) => res.status(200).end()).catch((e) => res.status(500).end());
})

module.exports = router;